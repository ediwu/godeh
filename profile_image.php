<?php
session_start();
 $id=$_SESSION['driver_id'];
 if (!isset($_SESSION['username'])) {
                                        header("Location: ../index.php");
                                        exit();
                                    }
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang=""> <!--<![endif]-->
<style>
.transparent {     
    background-color: Transparent;
    border: none;
    cursor:pointer;
    padding-left: 50px;
}
</style>

<?php 
include 'head.php';
include 'includes/db.inc.php';

 ?>

<body>
        <?php
        include 'menu.php';
        include 'header.php';
        include 'footer.php';

        ?>

        <div class="col-lg-12 col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3"><?php echo $username_?></strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                		<table align="center">
                                	<tr>
                                		<td>
                                    		<img class="rounded-circle mx-auto d-block" src="images/drivers/<?php echo $image_ ?>" alt="Card image cap" style="height: 200px; width:200px;">
                                    	</td>

                                        <form action="includes/insert.inc.php" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                    	<td>
                                    		
							                          <input type="hidden" name="id" value="<?php echo $id; ?>">
							                          <div class="row form-group">
							                            <div class="col-12 col-md-9" style="padding-left: 50px;"><input type="file" id="file-input" name="image" class="form-control-file"></div>
							                          </div>
							                          	<div style="padding-left: 50px;"><button type="submit" name="update_image2" class="btn btn-primary btn-sm">Update</button> <a class="btn btn-danger btn-sm" href="profile.php">Cancel</a></div>
							                    
                                    	</td>
                                        </form>
                                	</tr>


                                    <tr>
                                    	
                                    		<td>
	                                    	<div style="padding-top: 40px;"><h3 class="text-sm-center mt-2 mb-1"><?php echo $first_name_ . " " . $last_name_; ?> </h3></div>
	                                    	</td>
	                                    	<td>
	                                    	<div style="padding-top: 40px;"><a name="name" class="transparent" href="profile_name.php"><i class="fa fa-edit"></i> Edit </a> </div>
                                    		</td>
	                                	
	                                    
                          			</tr>

                          			<tr>
                          				<td>
                                    		<div style="padding-top: 40px;"><h4 class="text-sm-center mt-2 mb-1"><?php if($role_ == 1){echo "Admin";} elseif($role_ == 0){echo "Guest";} ?></h4></div>
                                    	</td>
                                    </tr>

                                    <tr>
                                    	<td>
                                    		<div style="padding-top: 40px;" text-sm-center mt-2 mb-1"><i class="fa fa-envelope"> <?php echo $email_; ?></i></div>
                                    	</td>
                                    	<td>
                                    		<div style="padding-top: 40px;"><a name="email" class="transparent" href="profile_email.php"><i class="fa fa-edit"></i> Edit </a> </div>
                                    	</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label style="padding-left: 25px;padding-top: 40px;">Change your Password</label>
                                        </td>
                                        <td>
                                            <div style="padding-left: 25px;padding-top: 40px;"><a href="profile_password.php" class="btn btn-primary btn-sm"> Update </a></div>
                                        </td>
                                    </tr>

                                </table>
                                </div>
                                <hr>

                            </div>
                        </div>
                    </div>
</body>
</html>
